import axios from 'axios';
const artist = {};

artist.get = (req, res) => {
  const nameArtist = req.params.artist || '';

  axios
    .get(`https://www.vagalume.com.br/${nameArtist}/index.js`)
    .then(responseArtist => {
      res.status(200).json({
        success: true,
        ...responseArtist.data
      });
    })
    .catch(err => {
      res.status(500).json({
        message: err
      });
    });
};

export default artist;
