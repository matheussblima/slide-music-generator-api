import path from 'path';
import rand from 'generate-key';
import PPTX from 'nodejs-pptx';
import underscore from 'underscore';

const createSlideMusic = {};

createSlideMusic.create = async (req, res) => {
  const body = req.body || {};

  const filesDir = `${path.dirname(require.main.filename)}/downloads`;
  const fileId = rand.generateKey();

  makeMusicSlide(filesDir, fileId, body);

  res.status(200).json({
    success: true,
    fileId,
    ...body
  });
};

const makeMusicSlide = async (filesDir, fileId, musicInfo) => {
  const slidesDefault = {
    textColor: 'FFFFFF',
    backgroundColor: '000000',
    text: {
      x: 100,
      y: 0,
      cx: 500,
      cy: 500,
      fontSize: 32,
      textAlign: 'center',
      fontFace: 'Arial'
    }
  };

  const lyric = musicInfo.lyric || '';
  let verses = lyric.split('\n\n');

  let auxVerses = [];
  verses.forEach(verse => {
    const rowsVerse = verse.split('\n');

    if (rowsVerse.length >= 8) {
      const versesJoin = underscore.chunk(rowsVerse, 4);
      versesJoin.forEach(row => {
        const newRow = row.join(' ');
        auxVerses.push(newRow);
      });
      return;
    }
    auxVerses.push(verse.replace('\n', ' '));
  });

  verses = auxVerses;

  let pptx = new PPTX.Composer();

  await pptx.compose(pres => {
    pres.addSlide(slide => {
      slide.textColor(slidesDefault.textColor);
      slide.backgroundColor(slidesDefault.backgroundColor);

      slide.addText(text => {
        text
          .value(`${musicInfo.artist} - ${musicInfo.music}`)
          .x(slidesDefault.text.x)
          .y(slidesDefault.text.y)
          .cx(slidesDefault.text.cx)
          .cy(slidesDefault.text.cy)
          .fontFace(slidesDefault.text.fontFace)
          .fontSize(slidesDefault.text.fontSize)
          .textAlign(slidesDefault.text.textAlign);
      });
    });

    verses.forEach(verse => {
      pres.addSlide(slide => {
        slide.textColor(slidesDefault.textColor);
        slide.backgroundColor(slidesDefault.backgroundColor);

        slide.addText(text => {
          text
            .value(verse)
            .x(slidesDefault.text.x)
            .y(slidesDefault.text.y)
            .cx(slidesDefault.text.cx)
            .cy(slidesDefault.text.cy)
            .fontFace(slidesDefault.text.fontFace)
            .fontSize(slidesDefault.text.fontSize)
            .textAlign(slidesDefault.text.textAlign);
        });
      });
    });
  });

  await pptx.save(`${filesDir}/${fileId}.pptx`);
};

export default createSlideMusic;
