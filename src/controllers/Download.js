import path from 'path';
import fs from 'fs';

const download = {};

download.get = (req, res) => {
  const fileId = req.params.fileId || '';
  const filesDir = `${path.dirname(require.main.filename)}/downloads`;

  res.download(`${filesDir}/${fileId}.pptx`, 'slide.pptx', function(err) {
    if (err) {
      res.status(500).json({
        success: false,
        message: 'Erro ao efetuar o download'
      });
    } else {
      fs.unlink(`${filesDir}/${fileId}.pptx`, () => {});
    }
  });
};

export default download;
