import axios from 'axios';
import { vagalumeApi } from '../config';
const musicLyrics = {};

musicLyrics.get = (req, res) => {
  const nameArtist = req.query.artist || '';
  const musicArtist = req.query.music || '';

  axios
    .get(
      `https://api.vagalume.com.br/search.php?art=${nameArtist}&mus=${musicArtist}&apikey${vagalumeApi.keyApi}`
    )
    .then(responseArtist => {
      res.status(200).json({
        success: true,
        ...responseArtist.data
      });
    })
    .catch(err => {
      res.status(500).json({
        message: err
      });
    });
};

export default musicLyrics;
