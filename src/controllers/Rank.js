import axios from 'axios';
import { vagalumeApi } from '../config';
const rank = {};

Date.prototype.getWeek = function() {
  var dt = new Date(this.getFullYear(), 0, 1);
  return Math.ceil(((this - dt) / 86400000 + dt.getDay() + 1) / 7);
};

rank.get = (req, res) => {
  const today = new Date();

  const typeRank = req.query.type || 'art';
  const limit = req.query.limit || '20';
  const scope = req.query.scope || 'all';
  const period =
    req.query.period ||
    today.getFullYear().toString() +
      today
        .getWeek()
        .toString()
        .padStart(2, '0');

  axios
    .get(
      `https://api.vagalume.com.br/rank.php?type=${typeRank}&period=week&periodVal=${period}&scope=${scope}&limit=${limit}&apikey=${vagalumeApi.keyApi}`
    )
    .then(responseArtist => {
      if (typeRank === 'art') {
        res.status(200).json({
          success: true,
          ...responseArtist.data,
          art: {
            ...responseArtist.data.art,
            week: {
              ...responseArtist.data.art.week,
              all: responseArtist.data.art.week.all.map(value => {
                return {
                  ...value,
                  url: `/${value.url.slice(28)}`
                };
              })
            }
          }
        });
      } else {
        res.status(200).json({
          success: true,
          ...responseArtist.data
        });
      }
    })
    .catch(err => {
      res.status(500).json({
        message: err
      });
    });
};

export default rank;
