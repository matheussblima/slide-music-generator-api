import axios from 'axios';
import { removeAccent } from '../utility';
const search = {};

const generateUrlImage = docs => {
  const urlImage = docs.map(value => {
    const newUrl = value.url.replace('/', '').split('/')[0];
    return {
      ...value,
      pic_medium: `https://s2.vagalume.com/${newUrl}/images/${newUrl}.jpg`
    };
  });

  return urlImage;
};

search.get = (req, res) => {
  let querySearch = req.query.query || '';

  querySearch = removeAccent(querySearch);

  const artist = axios.get(
    `https://api.vagalume.com.br/search.art?q=${querySearch}`
  );

  const excerpt = axios.get(
    `https://api.vagalume.com.br/search.excerpt?q=${querySearch}`
  );

  const artmus = axios.get(
    `https://api.vagalume.com.br/search.artmus?q=${querySearch}`
  );

  Promise.all([artist, excerpt, artmus])
    .then(response => {
      const responseSearch = {};
      response.forEach((search, index) => {
        switch (index) {
          case 0:
            responseSearch.artist = search.data.response
              ? {
                  ...search.data,
                  response: {
                    ...search.data.response,
                    docs: generateUrlImage(search.data.response.docs)
                  }
                }
              : search.data;
            break;
          case 1:
            responseSearch.excerpt = search.data.response
              ? {
                  ...search.data,
                  response: {
                    ...search.data.response,
                    docs: generateUrlImage(search.data.response.docs)
                  }
                }
              : search.data;
            break;
          case 2:
            responseSearch.artmus = search.data.response
              ? {
                  ...search.data,
                  response: {
                    ...search.data.response,
                    docs: generateUrlImage(search.data.response.docs)
                  }
                }
              : search.data;
            break;
          default:
            break;
        }
      });

      res.status(200).json({
        success: true,
        ...responseSearch
      });
    })
    .catch(err => {
      res.status(500).json({
        message: 'Não foi possível realizar a busca'
      });
    });
};

export default search;
