import Artist from './Artist';
import MusicLyrics from './MusicLyrics';
import Search from './Search';
import Rank from './Rank';
import CreateSlideMusic from './CreateSlideMusic';
import Download from './Download';

export { Artist, MusicLyrics, Search, Rank, CreateSlideMusic, Download };
