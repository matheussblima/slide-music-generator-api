import express from 'express';
import { Artist } from '../controllers';

const app = express();

// GET
app.get('/artist/:artist', Artist.get);

export default app;
