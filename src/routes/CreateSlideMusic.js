import express from 'express';
import { CreateSlideMusic } from '../controllers';

const app = express();

// POST
app.post('/create-slide', CreateSlideMusic.create);

export default app;
