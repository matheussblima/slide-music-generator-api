import express from 'express';
import { Download } from '../controllers';

const app = express();

// GET
app.get('/download/:fileId', Download.get);

export default app;
