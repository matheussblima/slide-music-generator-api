import express from 'express';
import { MusicLyrics } from '../controllers';

const app = express();

// GET
app.get('/lyric', MusicLyrics.get);

export default app;
