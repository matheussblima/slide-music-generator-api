import express from 'express';
import cors from 'cors';
import { Rank } from '../controllers';

const app = express();
app.use(cors());

// GET
app.get('/rank', Rank.get);

export default app;
