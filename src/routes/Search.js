import express from 'express';
import { Search } from '../controllers';

const app = express();

// GET
app.get('/search', Search.get);

export default app;
