import Artist from './Artist';
import MusicLyrics from './MusicLyrics';
import Search from './Search';
import Rank from './Rank';
import CreateSlideMusic from './CreateSlideMusic';
import Download from './Download';

const loadAllRoutes = app => {
  app.use('/api', Artist);
  app.use('/api', MusicLyrics);
  app.use('/api', Search);
  app.use('/api', Rank);
  app.use('/api', CreateSlideMusic);
  app.use('/api', Download);
};

export default loadAllRoutes;
